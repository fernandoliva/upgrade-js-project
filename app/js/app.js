window.onload = function () {
  init(1);
}

const init = (id) => {
  getPokemons(id);
  showPokemons(id);
}

let $$pagination = document.querySelector('.pagination');
let totalPages = 156/6;
let counter = 1;
for (let x = 1; x <= totalPages; x++){
  const $$li = document.createElement('li');
  $$li.classList.add('page-item');
  const $$a = document.createElement('a');
  $$a.classList.add('page-link');
  $$a.href = '#';
  if (x === 1){
    $$a.setAttribute("onclick","init(1)");
  } else {
    $$a.setAttribute("onclick","showPokemons("+(counter += 6)+")");
  }
  $$a.innerText = x;
  $$li.appendChild($$a);
  $$pagination.appendChild($$li);
}


const getPokemons = async (id) => {
  const result = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
  const resultToJson = await result.json();
  return resultToJson;
}

const showPokemons = async (id) => {
  $$divRow.innerHTML = '';
  if (id === 151){
    const pokemon = await getPokemons(id);
    printPokemon(pokemon);
  } else {
    for (let i = id; i <= id + 5; i++) {
      const pokemon = await getPokemons(i);
      printPokemon(pokemon);
    }
  }
}

const $$container = document.querySelector('.container');
const $$divRow = document.createElement('div');
$$divRow.classList.add('row', 'p-5');
$$container.appendChild($$divRow);

const printPokemon = (pokemons) => {
  const name = pokemons.name;
  const imgURL = pokemons.sprites.other['official-artwork'].front_default;
  const exp = pokemons.base_experience;
  const hp = pokemons.stats[0].base_stat;
  const atk = pokemons.stats[1].base_stat;
  const def = pokemons.stats[2].base_stat;
  const atkX = pokemons.stats[3].base_stat;

  const $$divCol = document.createElement('div');
  const $$card = document.createElement('div');
  const $$cardBody = document.createElement('div');
  const $$title = document.createElement('h4');
  const $$hp = document.createElement('p');
  const $$exp = document.createElement('p');
  const $$atk = document.createElement('p');
  const $$def = document.createElement('p');
  const $$atkX = document.createElement('p');

  $$divCol.classList.add('col-lg-4', 'col-md-6', 'col-xs-12', 'mb-4');
  $$card.classList.add('card', 'p-4');
  $$card.appendChild($$title);
  $$img = document.createElement('img');
  $$img.classList.add('imgCard');
  $$divCol.appendChild($$card);
  $$card.appendChild($$img);
  $$img.src = imgURL;
  $$img.alt = name;
  $$cardBody.classList.add('card-body');
  $$card.appendChild($$cardBody);
  $$title.classList.add('card-title');
  $$title.innerText = name;
  $$exp.classList.add('card-text');
  $$exp.innerText = 'Exp: ' + exp;
  $$hp.classList.add('card-text');
  $$hp.innerText = 'HP: ' + hp;
  $$atk.classList.add('card-text');
  $$atk.innerText = 'Atk: ' + atk;
  $$def.classList.add('card-text');
  $$def.innerText = 'Def: ' + def;
  $$atkX.classList.add('card-text');
  $$atkX.innerText = 'Special Atk: ' + atkX;
  $$cardBody.appendChild($$hp);
  $$cardBody.appendChild($$exp);
  $$cardBody.appendChild($$atk);
  $$cardBody.appendChild($$def);
  $$cardBody.appendChild($$atkX);
  $$divRow.appendChild($$divCol);
}