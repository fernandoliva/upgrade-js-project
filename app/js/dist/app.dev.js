"use strict";

// const getPokemon = (id) => {
//   //You can use name, number, type, or ability in the url. 
//   //Example: pokemon/ditto/, pokemon/1/, type/3/ or ability/4/.
//   fetch(`https://pokeapi.co/api/v2/pokemon?limit=151`)
//     .then(response => response.json())
//     .then(data => {
//       console.log(data)
//     })
// }
window.onload = function () {
  init();
};

var init = function init() {
  var id, pokemon;
  return regeneratorRuntime.async(function init$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          id = 1;

        case 1:
          if (!(id <= 20)) {
            _context.next = 9;
            break;
          }

          _context.next = 4;
          return regeneratorRuntime.awrap(getPokemons(id));

        case 4:
          pokemon = _context.sent;
          printPokemon(pokemon);

        case 6:
          id++;
          _context.next = 1;
          break;

        case 9:
        case "end":
          return _context.stop();
      }
    }
  });
}; //DOM global variables


var $$container = document.querySelector('.container');
var $$divRow = document.createElement('div');
$$divRow.classList.add('row', 'p-5');
$$container.appendChild($$divRow);

var getPokemons = function getPokemons(id) {
  var result, resultToJson;
  return regeneratorRuntime.async(function getPokemons$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(fetch("https://pokeapi.co/api/v2/pokemon/".concat(id)));

        case 2:
          result = _context2.sent;
          _context2.next = 5;
          return regeneratorRuntime.awrap(result.json());

        case 5:
          resultToJson = _context2.sent;
          return _context2.abrupt("return", resultToJson);

        case 7:
        case "end":
          return _context2.stop();
      }
    }
  });
};

var printPokemon = function printPokemon(pokemons) {
  var name = pokemons.name;
  var imgURL = pokemons.sprites.other['official-artwork'].front_default;
  var exp = pokemons.base_experience;
  var hp = pokemons.stats[0].base_stat;
  var types = pokemons.types.forEach(function (element) {
    console.log('Type: ' + element.type.name);
  });
  var atk = pokemons.stats[1].base_stat;
  var def = pokemons.stats[2].base_stat;
  var atkX = pokemons.stats[3].base_stat;
  var $$divCol = document.createElement('div');
  var $$card = document.createElement('div');
  var $$cardBody = document.createElement('div');
  var $$title = document.createElement('h4');
  var $$type = document.createElement('p');
  var $$hp = document.createElement('p');
  var $$exp = document.createElement('p');
  var $$atk = document.createElement('p');
  var $$def = document.createElement('p');
  var $$atkX = document.createElement('p');
  $$divCol.classList.add('col-lg-4', 'col-md-6', 'col-xs-12', 'mb-3');
  $$card.classList.add('card');
  $$img = document.createElement('img');
  $$img.classList.add('imgCard');
  $$divCol.appendChild($$card);
  $$card.appendChild($$img);
  $$img.src = imgURL;
  $$img.alt = name;
  $$cardBody.classList.add('card-body');
  $$card.appendChild($$cardBody);
  $$title.classList.add('card-title');
  $$cardBody.appendChild($$title);
  $$title.innerText = name;
  $$type.classList.add('card-text');
  $$type.innerText = 'Type: ' + types;
  $$exp.classList.add('card-text');
  $$exp.innerText = 'Exp: ' + exp;
  $$hp.classList.add('card-text');
  $$hp.innerText = 'HP: ' + hp;
  $$atk.classList.add('card-text');
  $$atk.innerText = 'Atk: ' + atk;
  $$def.classList.add('card-text');
  $$def.innerText = 'Def: ' + def;
  $$atkX.classList.add('card-text');
  $$atkX.innerText = 'Special Atk: ' + atkX;
  $$cardBody.appendChild($$type);
  $$cardBody.appendChild($$hp);
  $$cardBody.appendChild($$exp);
  $$cardBody.appendChild($$atk);
  $$cardBody.appendChild($$def);
  $$cardBody.appendChild($$atkX);
  $$divRow.appendChild($$divCol);
};